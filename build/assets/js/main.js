/*
 * Swisscom PNT Living Styleguide Landing Page
 * Developer: Marcel Hadorn for Duotones LLC (duotones.ch)
 * All HTML/CSS code (c)2016 Duotones LLC. all rights reserved
 * Design, Data and Content (c)2016 Swisscom AG.
*/
jQuery.browser={},jQuery.browser.mozilla=/mozilla/.test(navigator.userAgent.toLowerCase())&&!/webkit/.test(navigator.userAgent.toLowerCase()),jQuery.browser.webkit=/webkit/.test(navigator.userAgent.toLowerCase()),jQuery.browser.opera=/opera/.test(navigator.userAgent.toLowerCase()),jQuery.browser.msie=/msie/.test(navigator.userAgent.toLowerCase()),jQuery.browser.msie11=!!navigator.userAgent.match(/Trident.*rv[ :]*11\./)
var globalSettings={spaceBetween:0,longSwipesRatio:.3,keyboardControl:!0,preloadImages:!1,lazyLoading:!0,lazyLoadingInPrevNext:!0,lazyLoadingOnTransitionStart:!0},horizontalSettings={direction:"horizontal",speed:500,resistance:!0,paginationClickable:!0,pagination:".swiper-pagination",nextButton:".swiper-button-next",prevButton:".swiper-button-prev"},horizontalSet=$.extend({},globalSettings,horizontalSettings,{mousewheelForceToAxis:!0,mousewheelSensitivity:1,freeMode:!1}),horizontalSetNoTransitions=$.extend({},globalSettings,horizontalSettings,{mousewheelForceToAxis:!1,mousewheelSensitivity:.5,freeMode:!0,freeModeMomentum:!0,freeModeMomentumRatio:10,freeModeMinimumVelocity:.01,freeModeSticky:!1}),horizontalSetIE=$.extend({},globalSettings,horizontalSettings,{mousewheelForceToAxis:!1,mousewheelSensitivity:1,freeMode:!1}),horizontalSetT=$.extend({},globalSettings,horizontalSettings,{mousewheelForceToAxis:!0,mousewheelSensitivity:1,freeMode:!1,paginationClickable:!1,pagination:!1,nextButton:!1,prevButton:!1,speed:2e3,autoplay:5e3,effect:"fade"})
if($.browser.msie||$.browser.msie11)if(Modernizr.csstransitions)var swiperV=new Swiper(".swiper-container-h",horizontalSetIE),swiperT=new Swiper(".swiper-container-t",horizontalSetIE)
else var swiperV=new Swiper(".swiper-container-h",horizontalSetNoTransitions),swiperT=new Swiper(".swiper-container-t",horizontalSetNoTransitions)
else var swiperV=new Swiper(".swiper-container-h",horizontalSet),swiperT=new Swiper(".swiper-container-t",horizontalSetT)
$(document).ready(function(){function e(){if(Modernizr.mq("screen and (min-width:768px)")){var e=$(".half.image img").height()-2
$(".half.text").css({"min-height":e+"px"})}else Modernizr.mq("screen and (max-width:767px)")&&(console.log("small-screen"),$(".half.text .description").each(function(){var e=$(this).outerHeight()+40
$(this).parent().css({"min-height":e+"px"})}))}var t,i=function(){$(".tab-content").each(function(){var e=280,t=$(".tabs").outerHeight()
e=$(".tab-content").outerHeight()>e?Modernizr.mq("screen and (max-width:374px)")?$(".tab-content").outerHeight()+t+50:$(".tab-content").outerHeight()+100:280,$(".principles > .container").css({"min-height":e+"px"})})}
$(window).resize(function(){clearTimeout(t),t=setTimeout(e,0),e(),i()}),$(window).on("load",function(){e(),i()})
var n,o=$("#nav"),r=(o.outerHeight(),o.find("a.anchor")),a=r.map(function(){var e=$($(this).attr("href"))
return e.length?e:void 0})
r.click(function(e){var t=$(this).attr("href"),i="#"===t?0:$(t).offset().top-68
a=r.map(function(){var e=$($(this).attr("href"))
return e.length?e:void 0}),$("html, body").stop().animate({scrollTop:i},700,"easeInOutCubic"),e.preventDefault(),c()}),$(window).scroll(function(){var e=$(this).scrollTop()+70,t=a.map(function(){return $(this).offset().top<e?this:void 0})
t=t[t.length-1]
var i=t&&t.length?t[0].id:""
n!==i&&(n=i,r.parent().removeClass("active").end().filter("[href='#"+i+"']").parent().addClass("active"))})
var s=function(){$(window).scrollTop()<$(window).height()/20?$(".navigation").addClass("inverted"):$(".navigation").removeClass("inverted")}
$(window).on("scroll",function(e){e.stopPropagation(),s()}),s()
var l=function(){$(".hamburger, .navigation, #nav-drawer").toggleClass("in"),$(".shift").toggleClass("out")},c=function(){$(".hamburger, .navigation, #nav-drawer").removeClass("in"),$(".shift").removeClass("out")}
$(".hamburger").on("click",function(){l()}),$("li.tab a").on("click",function(){var e=$(this).data("tab")
$("li.tab").removeClass("active"),$(".tab-content").removeClass("active"),$(this).parent().addClass("active"),$("#"+e+".tab-content").addClass("active")})})
