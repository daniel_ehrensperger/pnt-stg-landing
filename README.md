# Static Site with Harp.js

## Requirements

* npm (node.js)
* bourbon
* neat
* bitters

## Installation
```
$ sudo npm install -g harp

$ cd <repo root>/public/assets/css

$ bourbon install
$ neat install
$ bitters install

```

## Usage
To run use

```
// In <repo root>
$ npm run dev

// Build
$ npm run build
```