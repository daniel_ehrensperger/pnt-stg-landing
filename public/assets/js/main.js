/*
 * Swisscom PNT Living Styleguide Landing Page
 * Developer: Marcel Hadorn for Duotones LLC (duotones.ch)
 * All HTML/CSS code (c)2016 Duotones LLC. all rights reserved
 * Design, Data and Content (c)2016 Swisscom AG.
*/

// GEt Browser
jQuery.browser = {};
jQuery.browser.mozilla = /mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit/.test(navigator.userAgent.toLowerCase());
jQuery.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase());
jQuery.browser.opera = /opera/.test(navigator.userAgent.toLowerCase());
jQuery.browser.msie = /msie/.test(navigator.userAgent.toLowerCase());
jQuery.browser.msie11 = !!navigator.userAgent.match(/Trident.*rv[ :]*11\./);

// var nua = navigator.userAgent;
// var is_android = ((nua.indexOf('Mozilla/5.0') > -1 && nua.indexOf('Android ') > -1 && nua.indexOf('AppleWebKit') > -1) && !(nua.indexOf('Chrome') > -1));

// if (is_android === false) {
//     $('html').addClass('not_android');
// }

// Global Swiper Sttings
var globalSettings = {
    spaceBetween: 0,
    longSwipesRatio: 0.3,
    keyboardControl: true,
    preloadImages: false,
    lazyLoading: true,
    lazyLoadingInPrevNext: true,
    lazyLoadingOnTransitionStart: true
};

// Global Horizontal Swiper Settings
var horizontalSettings = {
    direction: 'horizontal',
    //mousewheelControl: true,
    //mousewheelReleaseOnEdges: true,
    speed: 500,
    resistance: true,
    // hashnav: true,
    paginationClickable: true,
    pagination: '.swiper-pagination',
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
};


// Global + Vertical Swiper Settings
var horizontalSet = $.extend({}, globalSettings, horizontalSettings, {
    mousewheelForceToAxis: true,
    mousewheelSensitivity: 1,
    freeMode: false
});

// Global + Vertical Swiper Settings for Old Browsers
var horizontalSetNoTransitions = $.extend({}, globalSettings, horizontalSettings, {
    mousewheelForceToAxis: false,
    mousewheelSensitivity: 0.5,
    freeMode: true,
    freeModeMomentum: true,
    freeModeMomentumRatio: 10,
    freeModeMinimumVelocity: 0.01,
    freeModeSticky: false,
});

// Global + Vertical Swiper Settings for IE
var horizontalSetIE = $.extend({}, globalSettings, horizontalSettings, {
    mousewheelForceToAxis: false,
    mousewheelSensitivity: 1,
    freeMode: false
});

var horizontalSetT = $.extend({}, globalSettings, horizontalSettings, {
    mousewheelForceToAxis: true,
    mousewheelSensitivity: 1,
    freeMode: false,
    paginationClickable: false,
    pagination: false,
    nextButton: false,
    prevButton: false,
    speed: 2000,
    autoplay: 5000,
    effect: 'fade'
});

// Create Vertical Swiper

if ( !$.browser.msie && !$.browser.msie11 ) {
    var swiperV = new Swiper('.swiper-container-h', horizontalSet);
    var swiperT = new Swiper('.swiper-container-t', horizontalSetT);
} else if (!Modernizr.csstransitions) {
    var swiperV = new Swiper('.swiper-container-h', horizontalSetNoTransitions);
    var swiperT = new Swiper('.swiper-container-t', horizontalSetNoTransitions);
} else {
    var swiperV = new Swiper('.swiper-container-h', horizontalSetIE);
    var swiperT = new Swiper('.swiper-container-t', horizontalSetIE);
}

$(document).ready(function() {
    
    // if(window.location.pathname.length > 1) {
    //     //$('body').removeClass('home');
    //     //$('ul#nav').hide();
    // }

    var tabContentResize = function(){
        $('.tab-content').each(function(){
            var tabContent = 280,
				tabsHeight = $('.tabs').outerHeight();
            if ($('.tab-content').outerHeight() > tabContent) {
				if (Modernizr.mq("screen and (max-width:374px)")) {
                	tabContent = $('.tab-content').outerHeight() + tabsHeight + 50;
				} else {
					tabContent = $('.tab-content').outerHeight() + 100;
				}
            } else {
                tabContent = 280;
            }
            $('.principles > .container').css({'min-height':tabContent+'px'});
        });
    };
    
    //Function to react to screen re-sizing
    function elementResize() {
        if (Modernizr.mq("screen and (min-width:768px)")) {
            var img = $('.half.image img').height() - 2;
            $('.half.text').css({'min-height':img+'px'});
        }
        else if (Modernizr.mq("screen and (max-width:767px)")) {
            console.log('small-screen');
            $('.half.text .description').each(function(){
                var img = $(this).outerHeight() + 40;
                $(this).parent().css({'min-height':img+'px'});
            });
        }
    }
     
    // Call doneResizing on re-size of the window
    var id;
    $(window).resize(function () {
        clearTimeout(id);
        id = setTimeout(elementResize, 0);
        elementResize();
        tabContentResize();
    });
     
    // Call Functions on init
    $(window).on('load', function(){
        elementResize();
        tabContentResize();
    });
    
    // Cache selectors
    var lastId,
        topMenu = $("#nav"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a.anchor"),
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });
        

    // Bind click handler to menu items for animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-68;
          // Anchors corresponding to menu items
          scrollItems = menuItems.map(function(){
            var item = $($(this).attr("href"));
            if (item.length) { return item; }
          });
      $('html, body').stop().animate({ 
          scrollTop: offsetTop
      }, 700, "easeInOutCubic");
      e.preventDefault();
      closeNav();
    });

    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+70;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
       
       if (lastId !== id) {
           lastId = id;
           // Set/remove active class
           menuItems
             .parent().removeClass("active")
             .end().filter("[href='#"+id+"']").parent().addClass("active");
       }                   
    });
    
    var invertedNav = function(){
        if($(window).scrollTop() < $(window).height()/20){
           $('.navigation').addClass('inverted');
        } else {
            $('.navigation').removeClass('inverted');
        }
    };
    
    // Navigation Invert
    $(window).on('scroll', function(e){
        e.stopPropagation();
        
        invertedNav();
    });
    
    invertedNav();
    
    // Hamburger function show/hide nav
    var shiftNav = function(){
        $('.hamburger, .navigation, #nav-drawer').toggleClass('in');
        $('.shift').toggleClass('out');
    }
    
    var closeNav = function(){
        $('.hamburger, .navigation, #nav-drawer').removeClass('in');
        $('.shift').removeClass('out');
    }
    
    // Call Hamburger function
    $('.hamburger').on('click', function(){
        shiftNav();
    });
    
    $('li.tab a').on('click', function(){
        var active = $(this).data('tab');
        $('li.tab').removeClass('active');
        $('.tab-content').removeClass('active');
        $(this).parent().addClass('active');
        $('#'+active+'.tab-content').addClass('active');
    });
});